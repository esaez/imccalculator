import { StyleSheet } from 'react-native';

export const globalStyles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
  },
  img: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
    alignContent: 'center',
    alignItems: 'center'
  },
  primaryTitleText: {
    fontSize: 35,
    fontWeight: 'bold',
    color: '#ddd',
    textAlign: 'center'
  },
  subcontainer: {
    width: '90%',
    marginTop: 50,
    marginBottom: 50,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: "center",
  },
  label: {
    fontSize: 20,
    color: '#ddd'
  },
  input: {
    width: '80%',
    height: 50,
    borderWidth: 2,
    borderColor: '#ddd',
    borderRadius: 10,
    fontSize: 10,
    color: '#ddd',
    marginTop: 10,
    marginBottom: 20,
    padding: 5,
    fontSize: 20,
  },
  content:{
    marginTop: 20,
    padding:20,
    backgroundColor: '#FF5722',
    borderRadius:10,
  },
  btnText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#ddd',
    textAlign: 'center'
  },
  bottom: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  bottomText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#ddd',
    textAlign: 'center'
  },
  flatList:{
    marginTop: 60
  },
  card:{
    width:'auto',
    height:'auto',
    backgroundColor: 'black',
    margin: 5,
    padding: 20,
    borderRadius: 20
  },
  cardText:{
    color:'#ddd'
  }
});
