import React from 'react';
import { View, Text, FlatList} from 'react-native';
import { globalStyles } from '../styles/globalStyles';

export default function Stadistics({navigation}) {

  console.log('Stadistics'+navigation.getParam('imc'))
  
  return (
    <View style={globalStyles.container}>
      <FlatList data={navigation.getParam('imc')} renderItem={({item})=> (
        <View style={globalStyles.card}>
          <Text style={globalStyles.cardText}> peso: {item.peso}</Text>
          <Text style={globalStyles.cardText}> estatura: {item.estatura}</Text>
          <Text style={globalStyles.cardText}> detalle: {item.detalle}</Text>
        </View>  
      )}
      />
    </View>
  );
}
