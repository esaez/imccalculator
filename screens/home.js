import React, {useState} from 'react';
import { StyleSheet, View, Text, ImageBackground, TouchableOpacity, Alert, Keyboard, TouchableWithoutFeedback} from 'react-native';
import { globalStyles } from '../styles/globalStyles';
import Navigator from '../routes/HomeStack';
import { TextInput } from 'react-native-gesture-handler';

export default function Home({navigation}) {

  const showAbout = () => {
    Alert.alert('About', "Created By => JSalgado / ESaez", [
      {text: 'Understood', onPress: () => console.log('alert closed')}
    ]);
  }
  
  const [imc, setText] = useState('');

  const addImc = (imc, peso, estatura, detalle) => {
      setText((prevImc) => {
        return[
          { imc: ''+imc, peso: peso, estatura:estatura, detalle:detalle, key: Math.random().toString()},
          ...prevImc
        ]
      })

      console.log({imc})
  }

  const getImc = () =>{
    if (this.kilos === undefined || this.kilos == '') {
      console.log('kilos undefined');
      Alert.alert('Resultado IMC', 'Ingrese su peso', [
        {text: 'Aceptar', onPress: () => console.log('alert closed')}
      ]);
      return;
    }
    if (this.centimetros === undefined || this.centimetros == '') {
      console.log('kilos undefined');
      Alert.alert('Resultado IMC', 'Ingrese su altura', [
        {text: 'Aceptar', onPress: () => console.log('alert closed')}
      ]);
      return;
    }

    // Cálculo
    var imc = (this.kilos / ((this.centimetros / 100) * (this.centimetros / 100)))
    var text;
    if (imc < 18.5) {
      text="Peso inferior al normal";
    } else if (imc <= 24.9) {
      text="Peso normal";
    } else if (imc <= 29.9) {
      text="Peso superior al normal";
    } else {
      text="Obesidad";
    }

    Alert.alert('Resultado IMC', text, [
      {text: 'Aceptar', onPress: () => console.log('alert closed')}
    ]);

    addImc(imc, this.kilos, this.centimetros, text);
  }

  return (
    <TouchableWithoutFeedback onPress={ () => {
      Keyboard.dismiss();
    }}>
      <View style={globalStyles.container}>
        <ImageBackground source={{
          uri: 'https://images.unsplash.com/photo-1571388208497-71bedc66e932?ixlib=rb-1.2.1&auto=format&fit=crop&w=900&q=60'}} 
          style={globalStyles.img}>
          <Text style={globalStyles.primaryTitleText}>Calcular IMC</Text>
          <View style={globalStyles.subcontainer}>
            <Text style={globalStyles.label}>Ingrese su Peso (kg)</Text>
              <TextInput 
                style={globalStyles.input}
                placeholder='80'
                placeholderTextColor="rgba(255, 255, 255, 0.5)"
                keyboardType='numeric'
                onChangeText={(text) => this.kilos = text}
              />
            <Text style={globalStyles.label}>Ingrese su Altura (cm)</Text>
              <TextInput 
                style={globalStyles.input}
                placeholder='180'
                placeholderTextColor="rgba(255, 255, 255, 0.5)"
                keyboardType='numeric'
                onChangeText={(text) => this.centimetros = text}
              />
            <TouchableOpacity onPress={getImc}>
              <View style={globalStyles.content}>
                <Text style={globalStyles.btnText}>Obtener IMC</Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={globalStyles.bottom}>
            <TouchableOpacity onPress={()  => navigation.navigate('Stadistics', {imc})}>
              <View style={globalStyles.content}>
                <Text style={globalStyles.btnText}>Historial</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={showAbout}>
                <Text style={globalStyles.bottomText}>- About -</Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </View> 
      </TouchableWithoutFeedback>
  );
}
