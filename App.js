import React, { useState } from 'react';
import Navigator from './routes/HomeStack';

console.disableYellowBox = true;

export default function App() {
  return (
    <Navigator />
  );
};
