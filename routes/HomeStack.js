import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import Home from '../screens/home';
import Stadistics from '../screens/stadistics';

const screens = {
  Home: {
    screen: Home,
    navigationOptions:{
      title: 'Imc Calculator',
      header: null,
      headerStyle: { backgroundColor: '#03A9F4'},
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      headerTintColor: '#fff'
    }
  },
  Stadistics: {
    screen: Stadistics,
    navigationOptions:{
      title: 'Stadistics',
      headerStyle: { backgroundColor: '#03A9F4'},
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      headerTintColor: '#fff'
    }
  },
};

// home stack navigator screens
const HomeStack = createStackNavigator(screens);

export default createAppContainer(HomeStack);
